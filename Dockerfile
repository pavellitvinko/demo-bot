FROM python:2.7

RUN mkdir /root/.ssh/

COPY ./docker/ssh_keys/id_rsa /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

RUN pip install --upgrade pip==19.3.1
RUN pip install --upgrade setuptools

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
