import os

from logger import setup_logging
from werkzeug.serving import run_simple
from werkzeug.wsgi import DispatcherMiddleware

from botscape_bot.bot import webhooks as bot_webhooks
from botscape_bot.voice import app as voice_webhooks
from botscape_bot.api import app as api
from botscape_bot import frontend

setup_logging()

application = DispatcherMiddleware(frontend.create_app(), {
    '/webhooks': bot_webhooks.create_app(),
    '/voice': voice_webhooks.create_app(),
    '/api': api.create_app(),
})

if __name__ == '__main__':
    run_simple('0.0.0.0', port=5000, application=application,
               use_reloader=os.environ.get('AUTORELOAD') == '1',
               use_debugger=os.environ.get('DEBUG') == '1',
               threaded=True)
