from unittest import TestCase

from mock import patch

from botscape_bot.voice import webhooks
from botscape_bot.voice.replier import reply, session_closed


class TestVoice(TestCase):
    def setUp(self):
        self.user = {'id': 'test'}
        self.patcher = patch('botscape_bot.locales.service.locales')
        self.locales = self.patcher.start()
        self.locales.return_value = dict()
        self.app = webhooks.create_app()

    def test_translate_base_language(self):
        with self.app.app_context():
            session_closed(self.user)
            res = reply(self.user, 'translate hello to french')
            self.assertEqual(
                '<speak>$ALEXA_TRANSLATION$ <lang xml:lang="fr-FR"><voice name="Celine">Bonjour</voice></lang></speak>',
                res.speak_text)

    def test_translate_additional_language(self):
        with self.app.app_context():
            session_closed(self.user)
            res = reply(self.user, 'translate hello how are you to russian')
            self.assertIn('<speak>$ALEXA_TRANSLATION$ <audio src=', res.speak_text)

    def test_dialog_1(self):
        with self.app.app_context():
            session_closed(self.user)
            res = reply(self.user, 'start dialog')
            self.assertEqual('$ALEXA_SELECT_LANGUAGE$', res.speak_text)
            self.assertEqual('$ALEXA_SELECT_LANGUAGE_HELP$', res.ask_text)
            self.assertEqual(False, res.should_end_session)

    def test_dialog_2(self):
        with self.app.app_context():
            res = reply(self.user, 'German')
            self.assertEqual('$ALEXA_DIALOG_START$', res.speak_text)
            self.assertEqual('$ALEXA_SAY_NEXT$', res.ask_text)
            self.assertEqual(False, res.should_end_session)

    def test_dialog_3(self):
        with self.app.app_context():
            res = reply(self.user, 'Cold frost and sunshine day of wonder!')
            self.assertEqual(
                '<speak>$ALEXA_TRANSLATION$ <lang xml:lang="de-DE"><voice name="Hans">Kaltfrost und Sonnentag des Wunders!</voice></lang></speak>',
                res.speak_text)
            self.assertEqual('$ALEXA_SAY_NEXT$', res.ask_text)
            self.assertEqual(False, res.should_end_session)

    def test_dialog_wrong_lang(self):
        MESSAGES = [
            'start dialog',
            'fingalian',
            'german',
            'hello world'
        ]
        with self.app.app_context():
            session_closed(self.user)
            replies = []
            for line in MESSAGES:
                replies.append(reply(self.user, line))

            self.assertResponse('$ALEXA_SELECT_LANGUAGE$', '$ALEXA_SELECT_LANGUAGE_HELP$', False, replies[0])
            self.assertResponse('$ALEXA_ERROR_INVALID_LANGUAGE$', '$ALEXA_SELECT_LANGUAGE_HELP$', False, replies[1])
            self.assertResponse('$ALEXA_DIALOG_START$', '$ALEXA_SAY_NEXT$', False, replies[2])
            self.assertResponse('<speak>$ALEXA_TRANSLATION$ <lang xml:lang="de-DE"><voice name="Hans">Hallo '
                                'Welt</voice></lang></speak>', '$ALEXA_SAY_NEXT$', False, replies[3])

    def assertResponse(self, expected_speak_text, expected_ask_text, expected_should_end_session, actual):
        self.assertEqual(expected_speak_text, actual.speak_text)
        self.assertEqual(expected_ask_text, actual.ask_text)
        self.assertEqual(expected_should_end_session, actual.should_end_session)

    def tearDown(self):
        self.patcher.stop()
