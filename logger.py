import os

import logging
from logging.config import fileConfig

import yaml


def setup_logging(
        default_path='logging.yml',
        default_level=logging.DEBUG,
        env_key='LOG_CFG'
):
    """Setup logging configuration

    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
        logging.info("Loaded logging configuration from {}".format(path))
    else:
        logging.basicConfig(level=default_level)
