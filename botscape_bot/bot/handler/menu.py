import datetime
import logging

from .base import AbstractRequestHandler

from ..menu import menu_handler, state_handler
from ..services import chat_state

logger = logging.getLogger(__name__)


class MenuHandler(AbstractRequestHandler):
    def handle_request(self, request):
        state = chat_state.get_state(request.chat)
        menu_handler.reset()

        menu_handler.handle(request, request.text)
        if not menu_handler.is_proceed():
            state_handler.execute(state, request, request.text)
            if not state_handler.is_proceed():
                return super(MenuHandler, self).handle_request(request)

