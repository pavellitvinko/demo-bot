class BaseFactory(object):
    def __init__(self):
        self._handlers = []

    def add(self, handler):
        self._handlers.append(handler)
        return self

    def build(self):
        if len(self._handlers) == 0:
            return

        for i, handler in enumerate(self._handlers):
            if i != len(self._handlers) - 1:
                handler.set_next(self._handlers[i + 1])
        return self._handlers[0]
