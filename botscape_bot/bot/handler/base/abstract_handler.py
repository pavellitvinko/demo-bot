from abc import ABCMeta, abstractmethod


class AbstractRequestHandler(object):
    """
    The default chaining behavior can be implemented inside a base handler
    class.
    """

    __metaclass__ = ABCMeta

    _next_handler = None

    def set_next(self, handler):
        self._next_handler = handler
        return handler

    @abstractmethod
    def handle_request(self, *args, **kwargs):
        if self._next_handler:
            return self._next_handler.handle_request(*args, **kwargs)

        return None
