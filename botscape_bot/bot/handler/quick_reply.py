from ..locales import localize


class QuickReplyHandler:
    def __init__(self):
        self._replies = dict()
        self._is_proceed = False
        self._default_handler = None

    def register_reply_handler(self, reply, handler):
        self._replies[reply] = handler

    def register_default_handler(self, default_handler):
        self._default_handler = default_handler

    def handle(self, request, reply, *args, **kwargs):
        self._is_proceed = False
        if request.chat:
            for item in self._replies:
                if reply == localize(item, request.chat):
                    self._is_proceed = True
                    return self._replies[item](request, *args, **kwargs)

        if self._default_handler:
            self._is_proceed = True
            return self._default_handler(request, *args, **kwargs)

    def is_proceed(self):
        return self._is_proceed

    def reset(self):
        self._is_proceed = False
