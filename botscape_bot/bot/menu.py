from boton.views import ImageContent, VoiceContent, AudioContent, FileContent

from .base.state_handler import StateHandler

from .controller.commands import translate, close_menu, limits, menu, examples, help, account, bookmarks, start
from .controller.root import languages_menu, locales_list, subscribed, feedback_received, translate_image, \
    speech_to_text, switch_lang, handle_language_dst, handle_language_src, handle_locale, handle_locale_onboarding, \
    translate_document
from .quick_reply_handler import QuickReplyHandler
from .services import chat_state


def _reset_state(func):
    def wrapped(request, *args, **kwargs):
        chat_state.reset(request.chat)
        return func(request, *args, **kwargs)

    return wrapped


# Menu commands are available from any point of conversation to solve potential issues caused by delayed processing
# (when user clicked menu twice, but the server did not manage to serve the first request yet)

menu_handler = QuickReplyHandler()
# QuickReply handler should have user argument
menu_handler.register_reply_handler('$START$', _reset_state(start))
menu_handler.register_reply_handler('$SWITCH_LANGUAGES_DISABLED$', _reset_state(switch_lang))
menu_handler.register_reply_handler('$SWITCH_LANGUAGES$', _reset_state(switch_lang))
menu_handler.register_reply_handler('$EXAMPLES_MODE_EXIT$', _reset_state(translate))
menu_handler.register_reply_handler('$MENU_CLOSE$', _reset_state(close_menu))
menu_handler.register_reply_handler('$MENU_LOCALE$', _reset_state(locales_list))
menu_handler.register_reply_handler('$MENU_ACCOUNT$', _reset_state(account))
menu_handler.register_reply_handler('$MENU_BOOKMARKS$', _reset_state(bookmarks))
menu_handler.register_reply_handler('$MENU_LIMITS$', _reset_state(limits))
menu_handler.register_reply_handler('$MENU_HELP$', _reset_state(help))
menu_handler.register_reply_handler('$MENU$', _reset_state(menu))

state_handler = StateHandler()
state_handler.register_handler('email_requested', subscribed)
state_handler.register_handler('feedback_requested', feedback_received)
state_handler.register_handler('handle_language_src', handle_language_src)
state_handler.register_handler('handle_language_dst', handle_language_dst)
state_handler.register_handler('handle_locale', handle_locale)
state_handler.register_handler('handle_locale_onboarding', handle_locale_onboarding)

content_handler = StateHandler()
content_handler.register_handler(ImageContent, translate_image)  # user, message
content_handler.register_handler(VoiceContent, speech_to_text)
content_handler.register_handler(AudioContent, speech_to_text)
content_handler.register_handler(FileContent, translate_document)
