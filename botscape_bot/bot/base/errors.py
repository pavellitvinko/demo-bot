class BotException(Exception):
    def __init__(self, message, **params):
        self.message = message
        self.params = params

    def __str__(self):
        return self.message


class ActionNotFound(BotException):
    def __init__(self, action):
        super(ActionNotFound, self).__init__('Action "{}" not exists'.format(action))


class LinkNotFound(BotException):
    def __init__(self):
        super(LinkNotFound, self).__init__(u'Link not found')
