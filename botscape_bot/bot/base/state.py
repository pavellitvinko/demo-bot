from .models import State
from ...core import Service


class BotStateService(Service):
    __model__ = State

    def set(self, chat, new_state):
        model = self.first(chat=chat)
        if model:
            return self.update(model, state=new_state)

        return self.create(chat=chat, state=new_state)

    def reset(self, chat):
        model = self.first(chat=chat)
        if model is not None:
            return self.delete(model)

    def get_state(self, chat):
        m = self.first(chat=chat)
        if m is not None:
            return m.state
