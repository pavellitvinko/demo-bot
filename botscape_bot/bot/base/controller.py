from .errors import ActionNotFound


class Controller(object):
    def __init__(self):
        self._actions = {}

    def action(self, func):
        """Decorator that adds an action. Function name is used for action name."""
        name = func.__name__
        self._actions[name] = func
        return func

    def has_action(self, name):
        """Checks is controller has an action.

        Arguments:
            name(str): Action name.

        Returns:
            bool: True, if action exists, otherwise False.
        """
        return name in self._actions

    def process_action(self, name, chat, *args, **kwargs):
        """Processes action with arguments.

        Arguments:
            name(str): Action name.
            chat(Chat): User object
            *args, **kwargs: Action arguments.

        Raises:
            ActionNotFound

        Returns:
            Action result.
        """
        try:
            action = self._actions[name]
        except KeyError:
            raise ActionNotFound(name)

        return action(chat, *args, **kwargs)
