from __future__ import unicode_literals

import logging
from datetime import datetime

from botscape_bot.langs import ENGLISH_CODE
from settings import SESSION_TIMEOUT
from ...services import chats, profiles, chat_users, configs, subscriptions

logger = logging.getLogger(__name__)

DEFAULT_LOCALE = 'en'
CHAT_BOT_PLATFORM = 'chatBot'
SLACK_PLATFORM = 'slackBot'
VOICE_PLATFORM = 'voice'

class Request:
    def __init__(self, channel, update):
        self.created_time = datetime.now()
        self.channel = channel
        self.update = update
        self.chat = None
        self.user = None
        self.profile = None
        self.config = None
        self.subscription = None
        self.is_new_user = True
        if channel.chat:
            self.set_chat(chats.from_channel(channel))
        if channel.user:
            self.set_user(chat_users.get_or_create(channel.user))
        self.platform = _get_platform(channel.messenger_name)
        self.text = None  # FIXME: Used in UpdateHandler

    def set_chat(self, chat):
        self.chat = chat
        self.config = configs.get_or_create(self.chat, lang_dst=ENGLISH_CODE, locale=DEFAULT_LOCALE)
        self.platform = _get_platform(chat.messenger_name)
        self.subscription = _find_subscription(self.chat, self.platform)

    def set_user(self, user):
        self.user = user
        self.profile = profiles.get_for_user(self.user)
        if self.chat and (user not in self.chat.users):
            self.chat.users.append(self.user)
        self.is_new_user = self.profile is None

    @property
    def user_mention(self):
        if self.user is None:
            return

        if self.chat.messenger_name == 'slack':
            return '<@{0}>'.format(self.user.identifier['user_id'])
        elif self.chat.messenger_name == 'telegram' and self.profile:
            if self.profile.username and len(self.profile.username) > 0:
                return '@{0}'.format(self.profile.username)
            elif self.profile.first_name:
                return '{0}'.format(self.profile.first_name)
        else:
            if self.profile:
                return self.profile.username
            else:
                return self.user.values['name']

    @property
    def is_user_signed_in(self):
        return self.profile is not None and self.profile.botscape_account is not None


def _get_platform(messenger_name):
    if messenger_name == 'slack':
        return SLACK_PLATFORM
    elif messenger_name in ['telegram', 'facebook', 'vk', 'viber']:
        return CHAT_BOT_PLATFORM
    elif messenger_name in ['google_assistant', 'alexa']:
        return VOICE_PLATFORM


def _find_subscription(chat, platform):
    account_ids = []
    for user in chat.users:
        profile = profiles.get_for_user(user)
        if profile and profile.botscape_account:
            account_ids.append(profile.botscape_account.id)

    subscription_list = subscriptions.find_active_subscriptions(account_ids, platform)
    if len(subscription_list) > 0:
        return subscription_list[0]
