from datetime import datetime

from ...core import db


class BotLink(db.Model):
    __tablename__ = 'bot_links'

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    action = db.Column(db.Text(), nullable=False)
    params = db.Column(db.Text(), nullable=False)
    create_time = db.Column(db.DateTime(), default=datetime.now)


class State(db.Model):
    __tablename__ = 'state'

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    chat_id = db.Column(db.ForeignKey('chats.id'), nullable=False, unique=True)
    state = db.Column(db.Text(), nullable=True)

    chat = db.relationship('Chat')
