class StateHandler:
    def __init__(self):
        self._states = dict()
        self._default_handler = None
        self._is_proceed = False

    def register_handler(self, state, handler):
        self._states[state] = handler

    def register_default_handler(self, default_handler):
        self._default_handler = default_handler

    def execute(self, state, *args, **kwargs):
        self._is_proceed = False
        if state in self._states:
            self._is_proceed = True
            return self._states[state](*args, **kwargs)
        if self._default_handler:
            return self._default_handler(*args, **kwargs)

    def is_proceed(self):
        return self._is_proceed
