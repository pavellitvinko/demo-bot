import json

from .utils import unicode_escape
from .models import BotLink
from .errors import LinkNotFound
from ...core import Service


class BotLinksService(Service):
    __model__ = BotLink
    __not_found_exception__ = LinkNotFound

    def make_link(self, action, **params):
        """Creates short link for provided link data.

        Arguments:
            controller(str): Controller name.
            action(str): Action name.
            **params: Action params, json-serializable.

        Returns:
            (str): Short link.
        """
        model = self.get_link(action, **params)
        return str(model.id)

    def get_link(self, action, **params):
        """Creates short link for provided link data.

        Arguments:
            **params: Action params, json-serializable.

        Returns:
            (Link): Link model
        """

        params_json = json.dumps(params)
        return self.create(action=action, params=params_json)

    def expand_link(self, short_link):
        """Loads link model corresponding to short link.

        Arguments:
            short_link(str): Short link.

        Returns:
            Link model.
        """
        item = self.get_or_not_found(int(short_link))
        return item.action, json.loads(item.params)
