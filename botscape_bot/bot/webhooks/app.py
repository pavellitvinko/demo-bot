from botscape_bot import factory


def create_app(settings_override=None):
    app = factory.create_app(__name__, settings_override)

    from .facebook import bp as facebook
    from .telegram import bp as telegram
    from .viber import bp as viber
    from .vk import bp as vk
    from .slack import bp as slack

    app.register_blueprint(facebook)
    app.register_blueprint(telegram)
    app.register_blueprint(viber)
    app.register_blueprint(vk)
    app.register_blueprint(slack)

    return app
