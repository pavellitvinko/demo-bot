from .base import BotLinksService, BotStateService

chat_state = BotStateService()
links = BotLinksService()
