import logging

from boton.channels.viber import ServiceUpdate

from botscape_bot.langs import LanguageWrapper
from botscape_bot.configs import ChatConfig
from botscape_bot.locales import localize
from botscape_bot.services import configs, requests_limit, profiles, subscriptions, history
from .helpers import get_message_id_to_edit
from .instance import controller, error_handler
from .root import (get_examples, text_to_speech, get_meaning, translate_text, write_feedback,
                   locales_list, set_lang, set_locale)
from .slack import translate_dialog
from .. import views
from ..events import bot_events
from ..backend_factory import get_backenster

logger = logging.getLogger(__name__)


@controller.action
@error_handler
def lang(request, *args):
    bot_events.notify('lang', request=request, args=' '.join(args))

    if len(args) == 1:
        return set_lang(request, 'dst', args[0])
    elif len(args) == 2:
        set_lang(request, 'src', args[0])
        return set_lang(request, 'dst', args[1])
    else:
        return '$LANG_COMMAND_HELP$'


def _parse_args(cmd_line):
    args = cmd_line.split('_')
    res = {}
    for arg in args:
        if arg.startswith('ref'):
            res['ref'] = arg[3:]
        elif arg.startswith('lc'):
            res['locale'] = arg[2:]
    return res

@controller.action
@error_handler
def start(request, *args):
    cmd_line = ' '.join(args)
    args = _parse_args(cmd_line)

    locale = args.get('locale')
    bot_events.notify('start', request=request, cmd_line=cmd_line, ref=args.get('ref'), locale=locale)

    if locale:
        set_locale(request, locale, onboarding=True, silent=True)

    if request.chat.messenger_name == 'viber':
        if isinstance(request.update, ServiceUpdate):
            is_subscribed = request.update.raw.subscribed
        else:
            is_subscribed = True
        return views.send_view('welcome_viber.yml', request.chat, request.user,
                               user_id=request.user.identifier.get('user_id'), is_subscribed=is_subscribed)

    views.send_view('welcome.yml', request.chat, request.user, user_mention=request.user_mention)
    if (request.chat.messenger_name != 'slack') and (not locale):
        return locales_list(request, onboarding=True, message='$ONBOARDING_SELECT_LOCALE$')


@controller.action
@error_handler
def menu(request, *args):
    bot_events.notify('menu', request=request, args=' '.join(args))
    # chat_state.set(request.chat, 'menu')
    return views.send_view('menu_items.yml', request.chat, request.user)


@controller.action
@error_handler
def login(request, *args, **kwargs):
    if kwargs.get('do_not_track') is None:
        bot_events.notify('login', request=request, args=' '.join(args))

    edit_message_id = None
    if not kwargs.get('disable_editing', False):
        edit_message_id = get_message_id_to_edit(request)

    login_url = get_backenster(request).botscape_api.get_login_url(request.profile.uuid, request.chat.messenger_name)

    message = '$LOGIN_TITLE$'
    if 'message' in kwargs:
        message = kwargs['message']

    return views.send_view('account/login.yml', request.chat, request.user, login_url=login_url,
                           edit_message=edit_message_id, message=message)


@controller.action
@error_handler
def logout(request, *args):
    if request.profile.botscape_account:
        request.profile.botscape_account = None
        # Do not remove the account - it can be used on the other platforms
        profiles.save(request.profile)
        bot_events.notify('logout', request=request, was_signed_in=True, args=' '.join(args))
        return login(request, message='$LOGOUT_SUCCESS$', do_not_track=True)
    else:
        bot_events.notify('logout', request=request, was_signed_in=False, args=' '.join(args))
        return login(request, message='$LOGOUT_SUCCESS$', do_not_track=True)


@controller.action
@error_handler
def account(request, *args):
    bot_events.notify('account', request=request, args=' '.join(args))

    if not request.is_user_signed_in:
        limits(request, do_not_track=True)
        if request.chat.is_private or request.chat.messenger_name == 'slack':
            return login(request, disable_editing=True, do_not_track=True)
        else:
            return request.user_mention + ' ' + '$LOGIN_PRIVATE_PROMPT$'

    edit_message_id = get_message_id_to_edit(request)

    if request.chat.is_private or request.chat.messenger_name == 'slack':
        views.send_view('account/info.yml', request.chat, request.user, account=request.profile.botscape_account,
                        edit_message=edit_message_id, has_subscription=request.subscription is not None)
        limits(request, do_not_track=True)
        return views.send_view('account/manage.yml', request.chat, request.user,
                               account=request.profile.botscape_account,
                               edit_message=edit_message_id, has_subscription=request.subscription is not None)
    else:
        limits(request, do_not_track=True)
        return request.user_mention + ' ' + '$MANAGE_PRIVATE_PROMPT$'


@controller.action
@error_handler
def meaning(request, *args):
    bot_events.notify('meaning', request=request, args=' '.join(args))
    configs.set_mode(request.chat, ChatConfig.Mode.TRANSLATION)
    if len(args) > 0:
        return get_meaning(request, args[0])

    return views.send_view('translation/mode_activated.yml', request.chat, request.user)


@controller.action
@error_handler
def translate(request, *args):
    bot_events.notify('translate', request=request, args=' '.join(args))
    configs.set_mode(request.chat, ChatConfig.Mode.TRANSLATION)

    # FIXME: Refactor this logic
    if len(args) > 1:
        if len(args[0]) == 2:  # The first argument is looks like a language code
            lang_dst = get_backenster(request).find_supported_language(args[0])
            if lang_dst is None:
                # Invalid code or not a language code at all ("He ...")
                if request.channel.messenger_name == 'slack':
                    text = ' '.join(args)
                    return translate_dialog(request, text)
                else:
                    lang_dst = request.config.lang_dst
                    text = ' '.join(args)
                    return translate_text(request, text, lang_dst=lang_dst)
            else:
                # Yes, the first argument is language code!
                text = ' '.join(args[1:])
                return translate_text(request, text, lang_dst=lang_dst)
        else: # The first argument is a word
            text = ' '.join(args)
            if request.channel.messenger_name == 'slack':
                return translate_dialog(request, text)
            else:
                lang_dst = request.config.lang_dst
                return translate_text(request, text, lang_dst=lang_dst)
    else: # One or No arguments
        text = ' '.join(args)

        if request.channel.messenger_name == 'slack':
            return translate_dialog(request, text)

        if len(args) == 0:
            return _translate_mode_activated(request)
        else:
            lang_dst = request.config.lang_dst
            return translate_text(request, text, lang_dst=lang_dst)


def _translate_mode_activated(request):
    return views.send_view('translation/mode_activated.yml', request.chat, request.user)


@controller.action
@error_handler
def examples(request, *args):
    bot_events.notify('examples', request=request, args=' '.join(args))
    config = request.config

    if config.lang_dst is None:
        return '$ERROR_SELECT_DST_LANGUAGE$'

    if config.lang_src is None:
        return '$ERROR_SELECT_SRC_LANGUAGE$'

    configs.set_mode(request.chat, ChatConfig.Mode.EXAMPLES)

    if len(args) != 0:
        return get_examples(request, ' '.join(args))

    return _examples_mode_activated(request)


def _examples_mode_activated(request):
    return views.send_view('examples/mode_activated.yml', request.chat, request.user)


@controller.action
@error_handler
def close_menu(request, *args):
    bot_events.notify('close_menu', request=request, args=' '.join(args))

    if request.config.mode == ChatConfig.Mode.EXAMPLES:
        return _examples_mode_activated(request)
    elif request.config.mode == ChatConfig.Mode.TRANSLATION:
        return _translate_mode_activated(request)


@controller.action
@error_handler
def say(request, *args):
    bot_events.notify('say', request, args=' '.join(args))

    return text_to_speech(request, ' '.join(args))


@controller.action
@error_handler
def rate(request, *args):
    bot_events.notify('rate', request=request, args=' '.join(args))

    return views.send_view('feedback/rate.yml', request.chat, request.user)


@controller.action
@error_handler
def feedback(request, *args):
    bot_events.notify('feedback', request, args=' '.join(args))

    return write_feedback(request)


@controller.action
@error_handler
def help(request, *args):
    bot_events.notify('help', request=request, args=' '.join(args))

    return views.send_view('help.yml', request.chat, request.user)


@controller.action
@error_handler
def locale(request, *args):
    bot_events.notify('locale', request=request, args=' '.join(args))

    if len(args) > 0:
        return set_locale(request, args[0])
    else:
        return locales_list(request)


@controller.action
@error_handler
def limits(request, *args, **kwargs):
    if kwargs.get('do_not_track') is None:
        bot_events.notify('limits', request=request, args=' '.join(args))

    has_subscription = False
    subscription_until = None
    message = localize('$STATS_FREE_PLAN$', request.chat)

    backenster_config = get_backenster(request).config

    if request.subscription is not None:
        has_subscription = True
        message = localize('$STATS_PREMIUM_PLAN$', request.chat)

        subscription_until = request.subscription.period_end

    total_requests = requests_limit.get_total_requests(request.chat)
    total_characters = requests_limit.get_total_characters(request.chat)

    today_requests = requests_limit.get_today_requests(request.chat)
    today_characters = requests_limit.get_today_characters(request.chat)

    characters_limit = backenster_config.get('CharactersPerRequestLimit')
    daily_characters_limit = backenster_config.get('DailyCharactersLimit')
    daily_requests_limit = backenster_config.get('DailyRequestsLimit')

    message = message.format(subscription_until=subscription_until,
                             total_requests=total_requests,
                             total_characters=total_characters,
                             today_requests=today_requests,
                             today_characters=today_characters,
                             daily_characters_limit=daily_characters_limit,
                             daily_requests_limit=daily_requests_limit,
                             characters_limit=characters_limit)

    return views.send_view('settings/limits.yml', request.chat, request.user,
                           has_subscription=has_subscription, message=message)


@controller.action
@error_handler
def reset_limits(request, *args):
    bot_events.notify('reset_limits', request=request, args=' '.join(args))
    requests_limit.reset(request.chat)
    return 'Done!'


@controller.action
def referral(request, ref=None, ref_source=None, referer_uri=None):
    args = _parse_args(ref)
    locale = args.get('locale')
    if locale:
        set_locale(request, locale, onboarding=True, silent=True)

    bot_events.notify('referral', request=request,
                      cmd_line=ref,
                      ref=args.get('ref'),
                      ref_source=ref_source,
                      referer_uri=referer_uri,
                      locale=locale)


@controller.action
@error_handler
def bookmark(request, *args):
    if not request.is_user_signed_in:
        bot_events.notify('bookmark', request=request, args=' '.join(args), error='User is not signed in')
        return login(request, message='$BOOKMARK_LOGIN$', disable_editing=True, do_not_track=True)

    item = history.get_last_item(request.chat, request.user)
    if item is not None:
        lang_dst = LanguageWrapper(item.lang_dst)
        lang_src = LanguageWrapper(item.lang_src)

        if item.bookmark_id is None:
            # Add bookmark
            bookmark_id = get_backenster(request).botscape_api.add_bookmark(item.original_text,
                                                                             item.translated_text,
                                                                             item.lang_dst, item.lang_src)
            if bookmark_id:
                item.bookmark_id = bookmark_id
                history.save(item)
                bot_events.notify('bookmark', request=request, args=' '.join(args), original_text=item.original_text,
                                  translated_text=item.translated_text, action='add')
                return views.send_view('bookmarks/added.yml', request.chat, request.user, item=item,
                                       item_lang_dst=lang_dst, item_lang_src=lang_src)

            bot_events.notify('bookmark', request=request, args=' '.join(args), original_text=item.original_text,
                              translated_text=item.translated_text, action='add', error='Unable to add bookmark')
            return '$ERROR_UNABLE_TO_ADD_BOOKMARK$'
        else:
            # Delete bookmark
            if get_backenster(request).botscape_api.delete_bookmark(item.bookmark_id):
                item.bookmark_id = None
                history.save(item)
                bot_events.notify('bookmark', request=request, args=' '.join(args),
                                  original_text=item.original_text,
                                  translated_text=item.translated_text, action='remove')
                return views.send_view('bookmarks/removed.yml', request.chat, request.user, item=item,
                                       item_lang_dst=lang_dst, item_lang_src=lang_src)

            bot_events.notify('bookmark', request=request, args=' '.join(args), original_text=item.original_text,
                              translated_text=item.translated_text, action='remove', error='Unable to remove bookmark')
            return '$ERROR_UNABLE_TO_REMOVE_BOOKMARK$'
    else:
        bot_events.notify('bookmark', request=request, args=' '.join(args), error='History is empty')
        return '$ERROR_BOOKMARK_EMPTY_HISTORY$'


@controller.action
@error_handler
def bookmarks(request, *args):
    if not request.is_user_signed_in:
        bot_events.notify('bookmarks', request=request, args=' '.join(args), error='User is not signed in')
        return login(request, message='$BOOKMARK_LOGIN$', do_not_track=True)

    bot_events.notify('bookmarks', request=request, args=' '.join(args))

    items = get_backenster(request).botscape_api.get_bookmarks()
    bookmarks = []
    if items is not None and len(items) > 0:
        for item in items:
            bookmarks.append({
                'lang_src': LanguageWrapper(item['languageFrom']),
                'lang_dst': LanguageWrapper(item['languageTo']),
            })
        return views.send_view('bookmarks/bookmarks_list.yml', request.chat, request.user, items=bookmarks)
    else:
        return '$BOOKMARK_LIST_EMPTY$'
