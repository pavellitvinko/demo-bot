import logging

from celery import Celery
from flask import Flask

from .core import db, cors

logger = logging.getLogger(__name__)


def create_app(package_name, settings_override=None):
    app = Flask(package_name, instance_relative_config=True)

    # Load the default configuration
    app.config.from_object('settings')

    db.init_app(app)
    cors.init_app(app)
    return app


def make_celery(app=None):
    app = app or create_app('botscape_bot')

    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    # noinspection PyPropertyAccess
    celery.Task = ContextTask
    return celery
