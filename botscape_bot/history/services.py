from .models import History
from ..core import Service


class HistoryService(Service):
    __model__ = History

    def add(self, chat, user, original_text, translated_text, lang_src, lang_dst):
        return self.create(chat_id=chat.id,
                           user_id=user.id,
                           original_text=original_text,
                           translated_text=translated_text,
                           lang_src=lang_src,
                           lang_dst=lang_dst)

    @staticmethod
    def get_last_item(chat, user):
        return (History.query
                .filter(History.chat_id == chat.id)
                .filter(History.user_id == user.id)
                .order_by(History.created_date.desc()).first())