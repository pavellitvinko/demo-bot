from datetime import datetime

from ..core import db


class History(db.Model):
    __tablename__ = 'history'

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    chat_id = db.Column(db.ForeignKey('chats.id'), nullable=False)
    chat = db.relationship('Chat', uselist=False)
    user_id = db.Column(db.ForeignKey('users.id'), nullable=False)
    user = db.relationship('ChatUser', uselist=False)
    bookmark_id = db.Column(db.Integer())
    original_text = db.Column(db.String(), nullable=False)
    translated_text = db.Column(db.String(), nullable=False)
    lang_src = db.Column(db.Text(), nullable=False)
    lang_dst = db.Column(db.Text(), nullable=False)
    created_date = db.Column(db.DateTime(), nullable=True, default=datetime.now)

    def __str__(self):
        return self.id
