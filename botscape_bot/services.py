from .history import HistoryService
from .users import ChatService, ChatUsersService, ChatUserRelService
from .configs import ConfigService
from .logs import UpdateLogService
from .tokens import SlackTokenService
from .subscriptions import SubscriptionService
from .profiles import ProfileService, AccountService
from .feedback import FeedbackService
from .requests_limit import RequestsLimitService
from .promo import (AdImpressionService, NotificationService, NotificationDisplayService,
                    NewsItemService, NewsDisplayService, AdBannerService)

chats = ChatService()
chat_users = ChatUsersService()
chat_user_rel = ChatUserRelService()

profiles = ProfileService()
accounts = AccountService()
configs = ConfigService()
history = HistoryService()
requests_limit = RequestsLimitService()
subscriptions = SubscriptionService()
feedback = FeedbackService()
slack_tokens = SlackTokenService()

ad_banners = AdBannerService()
ads_service = AdImpressionService()
news_items = NewsItemService()
news_service = NewsDisplayService()
notification_items = NotificationService()
notification_service = NotificationDisplayService()

update_log = UpdateLogService()
