from .models import ChatConfig
from ..core import Service
from ..profiles import ProfileService


class InvalidDirection(Exception):
    pass


class ConfigService(Service):
    __model__ = ChatConfig

    def __init__(self):
        self._profiles = ProfileService()

    def get_for_chat(self, chat):
        return self.first(chat_id=chat.id)

    def get_or_create(self, chat, lang_dst, locale):
        m = self.first(chat_id=chat.id)
        if m is None:
            return self.create(chat_id=chat.id, lang_dst=lang_dst, locale=locale)
        return m

    def set_language(self, chat, direction, code):
        """
        Raises:
            InvalidDirection if direction is not "src" or "dst"
        """
        m = self.first(chat_id=chat.id)
        if direction == 'src':
            m.lang_src = code
        elif direction == 'dst':
            m.lang_dst = code
        else:
            raise InvalidDirection()
        return self.save(m)

    def set_file_language(self, chat, direction, code):
        """
        Raises:
            InvalidDirection if direction is not "src" or "dst"
        """
        m = self.first(chat_id=chat.id)
        if direction == 'src':
            m.lang_src_file = code
        elif direction == 'dst':
            m.lang_dst_file = code
        else:
            raise InvalidDirection()
        return self.save(m)

    def set_mode(self, chat, mode):
        m = self.first(chat_id=chat.id)
        return self.update(m, mode=mode)

    def switch_languages(self, chat):
        m = self.first(chat_id=chat.id)
        src = m.lang_src or m.lang_src_last_detected
        if src is not None:
            m.lang_src = m.lang_dst
            m.lang_dst = src
            return self.save(m)

    def set_locale(self, chat, locale):
        m = self.first(chat_id=chat.id)
        return self.update(m, locale=locale)

    def set_locales(self, chat_ids, locale):
        return (ChatConfig.query
                .filter(ChatConfig.chat_id.in_(chat_ids))
                .update({"locale": locale}, synchronize_session='fetch'))

    def enable_translation(self, chat, is_enabled):
        m = self.first(chat_id=chat.id)
        return self.update(m, is_enabled=is_enabled)

    def set_limit_exceed(self, config, limit_exceed):
        config.limit_exceed = limit_exceed
        self.save(config)
