import enum
from ..langs import LanguageWrapper, ENGLISH_CODE, AUTO_LANGUAGE
from ..core import db


class ChatConfig(db.Model):
    __tablename__ = 'configs'

    class Mode(enum.Enum):
        TRANSLATION = 1
        EXAMPLES = 2

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    chat_id = db.Column(db.ForeignKey('chats.id'), nullable=False)
    locale = db.Column(db.Text(), nullable=False)
    lang_src_last_detected = db.Column(db.Text())  # Save full translation history instead?
    lang_src = db.Column(db.Text())
    lang_dst = db.Column(db.Text(), nullable=False)
    lang_src_file = db.Column(db.Text())        # Used to translate single file, image or website
    lang_dst_file = db.Column(db.Text())
    is_enabled = db.Column(db.Boolean(), nullable=False, default=True)
    mode = db.Column(db.Enum(Mode), default=Mode.TRANSLATION, nullable=False)
    limit_exceed = db.Column(db.Boolean(), nullable=False, default=False)
    chat = db.relationship('Chat')

    def __str__(self):
        return self.id
