import logging
from datetime import datetime

from .models import Notification, NewsItem, AdBanner
from ..core import Service

logger = logging.getLogger(__name__)


class PromoItemService(Service):
    def disable_items(self, exclude_ids):
        q = (self.__model__.query
             .filter(~self.__model__.server_id.in_(exclude_ids))
             .update({"is_active": False}, synchronize_session='fetch'))
        return q

    def update_items(self, items):
        fetched_ids = []
        has_new = False

        for item in items:
            server_id = self.get_server_id(item)
            fetched_ids.append(server_id)

            m = self.first(server_id=server_id)
            if m is not None:
                m.last_update = datetime.now()
                self.update(m, **self.get_fields(item))
            else:
                logger.debug('Loaded new {}'.format(str(self.__model__)))
                self.create(server_id=server_id, **self.get_fields(item))
                has_new = True

        self.disable_items(fetched_ids)
        return has_new

    def get_fields(self, item):
        raise NotImplementedError()

    def get_server_id(self, item):
        return item['id']


class NewsItemService(PromoItemService):
    __model__ = NewsItem

    def get_fields(self, item):
        return dict(
            is_active=True,
            content=item['content'],
            expiration_date=datetime.utcfromtimestamp(item['expireDate'] / 1000),
        )


class AdBannerService(PromoItemService):
    __model__ = AdBanner

    def get_fields(self, item):
        return dict(content=item, display_order=item['order'])

    def get_server_id(self, item):
        return int(item['index'])

    def get_next(self, last_index):
        m = (AdBanner.query
             .filter(AdBanner.display_order > last_index)
             .filter(AdBanner.is_active == True)
             .order_by(AdBanner.display_order)).first()
        if m is None:
            m = (AdBanner.query
                 .filter(AdBanner.display_order >= 0)
                 .filter(AdBanner.is_active == True)
                 .order_by(AdBanner.display_order)).first()
        return m


class NotificationService(PromoItemService):
    __model__ = Notification

    def get_fields(self, item):
        return dict(content=item['content'], is_active=True)

    def get_next(self, last_id):
        m = (Notification.query
             .filter(Notification.server_id > last_id)
             .filter(Notification.is_active == True)
             .order_by(Notification.server_id)).first()
        if m is None:
            m = (Notification.query
                 .filter(Notification.server_id >= 0)
                 .filter(Notification.is_active == True)
                 .order_by(Notification.server_id)).first()
        return m

