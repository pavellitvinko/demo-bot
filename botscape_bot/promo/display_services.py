import logging
from datetime import datetime
from operator import or_

from sqlalchemy import desc

from .models import (NotificationDisplay,
                     NewsItem, NewsDisplay,
                     AdImpression)
from .services import AdBannerService, NotificationService
from ..profiles import ProfileService
from ..core import Service, db

logger = logging.getLogger(__name__)


class DisplayService(Service):
    __model__ = None
    __item_name__ = None

    def add_show(self, chat, item):
        return self.create(chat_id=chat.id, **{self.__item_name__: item})

    def has_shown(self, chat, item):
        return self.first(chat_id=chat.id, **{self.__item_name__: item}) is not None

    def choose(self, user):
        raise NotImplementedError()


class NewsDisplayService(DisplayService):
    __model__ = NewsDisplay
    __item_name__ = 'news_item'

    def __init__(self):
        super(NewsDisplayService, self).__init__()

    def choose(self, chat):
        subq = (db.session.query(NewsDisplay.news_item_id)
                .filter(NewsDisplay.chat_id == chat.id))

        query = (db.session.query(NewsItem)
                 .filter(NewsItem.id.notin_(subq))
                 .filter(or_(NewsItem.expiration_date > datetime.now(), NewsItem.expiration_date == None)))

        return query.all()


class AdImpressionService(DisplayService):
    __model__ = AdImpression
    __item_name__ = 'banner'

    def __init__(self):
        super(AdImpressionService, self).__init__()
        self._banner_service = AdBannerService()

    def choose(self, chat):
        last_impression = (AdImpression.query
                           .filter(AdImpression.chat_id == chat.id)
                           .order_by(desc(AdImpression.display_time))).first()

        display_order = 0
        if last_impression is not None:
            display_order = last_impression.banner.display_order

        next_item = self._banner_service.get_next(display_order)
        if next_item is not None:
            return [next_item]
        return []


class NotificationDisplayService(DisplayService):
    __model__ = NotificationDisplay
    __item_name__ = 'notification'

    def __init__(self):
        super(NotificationDisplayService, self).__init__()
        self._notification_service = NotificationService()
        self._profiles = ProfileService()

    def can_show_notification(self, chat, interval, max_impressions):
        last_interaction = chat.last_interaction
        if (last_interaction is None) or (not chat.is_private):
            return False

        last_impression = (NotificationDisplay.query
                           .filter(NotificationDisplay.chat_id == chat.id)
                           .order_by(desc(NotificationDisplay.display_time))).first()

        if last_impression is None:
            return True

        total_impressions_since_last_interaction = (NotificationDisplay.query
                                                    .filter(NotificationDisplay.chat_id == chat.id)
                                                    .filter(NotificationDisplay.display_time > last_interaction)
                                                    .count())

        last_message = max(last_impression.display_time, last_interaction)
        if last_message + interval < datetime.now():
            if total_impressions_since_last_interaction < max_impressions:
                return True
        return False

    def choose(self, chat):
        last_impression = (NotificationDisplay.query
                           .filter(NotificationDisplay.chat_id == chat.id)
                           .order_by(desc(NotificationDisplay.display_time))).first()

        last_id = 0
        if last_impression is not None:
            last_id = last_impression.notification.server_id

        next_item = self._notification_service.get_next(last_id)
        if next_item is not None:
            return [next_item]
        return []
