import datetime
import json

from ..core import db


class AdBanner(db.Model):
    __tablename__ = 'ad_banners'

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    server_id = db.Column(db.Integer(), nullable=False)
    display_order = db.Column(db.Integer(), nullable=False, default=0)
    content = db.Column(db.JSON(), nullable=False)
    is_active = db.Column(db.Boolean(), nullable=False, default=True)
    fetch_date = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)
    last_update = db.Column(db.DateTime, nullable=True)
    impressions = db.relationship("AdImpression", back_populates="banner")


class Notification(db.Model):
    __tablename__ = 'notifications'

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    server_id = db.Column(db.Integer(), nullable=False)
    content = db.Column(db.JSON(), nullable=False)
    is_active = db.Column(db.Boolean(), nullable=False, default=False)
    last_update = db.Column(db.DateTime, nullable=True)
    fetch_date = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)
    impressions = db.relationship("NotificationDisplay", back_populates="notification")


class NewsItem(db.Model):
    __tablename__ = 'news_items'

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    server_id = db.Column(db.Integer(), nullable=False)
    content = db.Column(db.JSON(), nullable=False)
    is_active = db.Column(db.Boolean(), nullable=False, default=False)
    expiration_date = db.Column(db.DateTime, nullable=True)
    fetch_date = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)


class AdImpression(db.Model):
    __tablename__ = 'ad_impressions'

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    chat_id = db.Column(db.ForeignKey('chats.id'), nullable=False)
    banner_id = db.Column(db.ForeignKey('ad_banners.id'), nullable=False)

    display_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)

    chat = db.relationship('Chat', uselist=False)
    banner = db.relationship('AdBanner', uselist=False, back_populates="impressions")


class NewsDisplay(db.Model):
    __tablename__ = 'news_displays'

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    chat_id = db.Column(db.ForeignKey('chats.id'), nullable=False)
    news_item_id = db.Column(db.ForeignKey('news_items.id'), nullable=False)

    display_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)

    chat = db.relationship('Chat', uselist=False)
    news_item = db.relationship('NewsItem', uselist=False)


class NotificationDisplay(db.Model):
    __tablename__ = 'notification_displays'

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    chat_id = db.Column(db.ForeignKey('chats.id'), nullable=False)
    notification_id = db.Column(db.ForeignKey('notifications.id'), nullable=False)

    display_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)

    chat = db.relationship('Chat', uselist=False)
    notification = db.relationship('Notification', uselist=False)
