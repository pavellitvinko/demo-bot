from .models import Feedback
from ..core import Service


class FeedbackService(Service):
    __model__ = Feedback

    def add(self, chat_user, message=None, rating=None):
        return self.create(chat_user_id=chat_user.id, message=message, rating=rating)
