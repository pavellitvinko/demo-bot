from datetime import datetime

from ..core import db


class Feedback(db.Model):
    __tablename__ = 'feedback'

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    chat_user_id = db.Column(db.ForeignKey('users.id'), nullable=False)
    rating = db.Column(db.Integer, nullable=True)
    message = db.Column(db.Text())
    created_date = db.Column(db.DateTime(), nullable=False, default=datetime.now)

    chat_user = db.relationship('ChatUser', uselist=False)
