from datetime import datetime

from ..core import db


class SlackToken(db.Model):
    __tablename__ = 'slack_tokens'
    __table_args__ = (
        db.UniqueConstraint('team_id', 'user_id', name='team_user_uq'),
    )

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    team_id = db.Column(db.Text(), nullable=False)
    user_id = db.Column(db.Text(), nullable=False)
    scope = db.Column(db.Text())
    is_bot = db.Column(db.Boolean(), nullable=False, default=False)
    access_token = db.Column(db.Text(), nullable=False)
    created_date = db.Column(db.DateTime(), nullable=True, default=datetime.now)
    updated_date = db.Column(db.DateTime(), nullable=True, default=datetime.now)
