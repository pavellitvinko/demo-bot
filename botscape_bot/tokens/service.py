from datetime import datetime

from .models import SlackToken
from ..core import Service


class TokenNotFound(Exception):
    pass


class SlackTokenService(Service):
    __model__ = SlackToken
    __not_found_exception__ = TokenNotFound

    def add_or_update(self, team_id, user_id, scope, access_token, is_bot=False):
        m = self.first(team_id=team_id, user_id=user_id)
        if m is not None:
            self.update(m, scope=scope, access_token=access_token, is_bot=is_bot, updated_date=datetime.now())
            return m
        return self.create(team_id=team_id, user_id=user_id, scope=scope, access_token=access_token, is_bot=is_bot)

    def get_bot_token(self, team_id):
        tokens = self.find(team_id=team_id, is_bot=True).all()
        if len(tokens) > 0:
            return tokens[-1]  # Return latest

    def get_user_token(self, team_id, user_id):
        tokens = self.find(team_id=team_id, user_id=user_id).all()
        if len(tokens) > 0:
            return tokens[-1]  # Return latest

    def get_tokens_tuple(self, team_id, user_id):
        bot_token = self.get_bot_token(team_id)
        user_token = self.get_user_token(team_id, user_id)
        if bot_token is None and user_token is None:
            self._raise_not_found()

        return bot_token, user_token

    def get_bot_token_for_chat(self, chat):
        team_id = chat.identifier.get('team_id')
        if team_id is None:
            return
        return self.get_bot_token(team_id)

    def get_user_token_for_request(self, request):
        if request.user is None:
            return
        team_id = request.user.identifier.get('team_id')
        user_id = request.user.identifier.get('user_id')
        if team_id is None or user_id is None:
            return
        return self.get_user_token(team_id, user_id)

    def revoke_user_token(self, token):
        return self.delete(token)

    def revoke_bot_token(self, token):
        return self.delete(token)
