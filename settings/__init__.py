# Read more about settings structure here: https://djangostars.com/blog/configuring-django-settings-best-practices/
from .bot import *
from .celery import *
from .flask import *
