import environ

env = environ.Env()

DEBUG = env.bool('DEBUG', default=False)

SECRET_KEY = env.str('SECRET_KEY')
APP_SERVER_NAME = env.str('SERVER_NAME')

BASIC_AUTH_USERNAME = env.str('BASIC_AUTH_USERNAME', default='admin')
BASIC_AUTH_PASSWORD = env.str('BASIC_AUTH_PASSWORD', default='botscape-admin')
BASIC_AUTH_FORCE = env.str('BASIC_AUTH_FORCE', default=True)

AUTH_TOKEN = env.str('AUTH_TOKEN')
